// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDvViwTh-8n5dS-SheqPhdPS4iub-EC8Pk",
    authDomain: "login-angular5.firebaseapp.com",
    databaseURL: "https://login-angular5.firebaseio.com",
    projectId: "login-angular5",
    storageBucket: "login-angular5.appspot.com",
    messagingSenderId: "740372938969"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
