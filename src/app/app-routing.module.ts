import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './componentes/home/home.component'
import { LoginComponent } from './componentes/login/login.component'
import { RegistroComponent } from './componentes/registro/registro.component'
import { IntranetComponent } from './componentes/intranet/intranet.component'
import { NotFoundComponent } from './componentes/not-found/not-found.component'
import { AuthGuard } from './guards/auth.guard'

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'registro', component: RegistroComponent},
  {path: 'intranet', component: IntranetComponent, canActivate: [AuthGuard]},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
