import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  public email: string
  public pass: string

  constructor(public authService: AuthService, public router: Router) { }

  ngOnInit() {
  }

  addUser() {
    this.authService.registrar(this.email, this.pass)
    .then(res => {
      this.router.navigate(['/intranet'])
    })
    .catch(err => { 
      console.log('No funciona :(')
      console.log(err)
    })
  }
}
