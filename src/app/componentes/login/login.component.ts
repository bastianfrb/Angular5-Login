import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.scss'
  ]
})
export class LoginComponent implements OnInit {
  public email: string
  public pass: string

  constructor(public authService: AuthService, public router: Router) { }

  ngOnInit() {
  }

  login() {
    this.authService.loginEmail(this.email, this.pass)
    .then(res => this.router.navigate(['/intranet']))
    .catch(err => {
      console.log('no funka')
      console.log(err)
    })
  }

}
