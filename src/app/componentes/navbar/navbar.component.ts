import { Component, OnInit } from '@angular/core'
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public isLogin: boolean
  public usuario: string
  public email: string

  constructor(public authService: AuthService) { }

  ngOnInit() {
    this.authService.getAuth().subscribe(auth => {
      if(auth){
        this.isLogin = true
        this.usuario = auth.displayName
        this.email = auth.email
      }else{
        this.isLogin = false
      }
    })
  }

  logout() {
    this.authService.logout()
  }
}
