import { Injectable } from '@angular/core'
import { AngularFireAuth } from 'angularfire2/auth'
import * as firebase from 'firebase/app'
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afAuth: AngularFireAuth) {  }

  registrar(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, pass)
      .then( res => {resolve(res)}),
      err => {
        console.log(err)
        reject(err)
      }
    })
  }

  loginEmail(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, pass)
      .then(res => resolve(res)),
      err => {
        console.log(err)
        reject(err)
      }
    })
  }

  loginFacebook() {
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.FacebookAuthProvider()
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => resolve(res),
      err => {
        console.log(err)
        reject(err)
      })
    })
  }


  loginGoogle() {
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.GoogleAuthProvider()
      provider.addScope('profile')
      provider.addScope('email')
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => resolve(res))
    })
  }


  loginTwitter() {
    return new Promise<any>((resolve, reject) => {
      let proveedor = new firebase.auth.TwitterAuthProvider()
      this.afAuth.auth
      .signInWithPopup(proveedor)
      .then(res => resolve(res),
      err => {
        console.log(err)
        reject(err)
      })
    })
  }

  logout() {
    return this.afAuth.auth.signOut()
  }

  getAuth() {
    return this.afAuth.authState.pipe(map(auth => auth))
  }
  
}
